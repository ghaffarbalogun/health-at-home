# Bug Title
Verbose description of bug

### Expected Behavior
Description of expected behavior

### Encountered Behavior
Description of encountered behavior

### Steps to Reproduce
* step
* step

### Related issues
Name any related issues using `#number`