import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Home } from './Home'
import { About } from './About'
import { Recipes } from './Recipes'
import { Workouts } from './Workouts'
import { HealthNews } from './HealthNews'
import { NoMatch } from './NoMatch'
import {Layout} from './Components/Layout'
import {NavigationBar} from './Components/NavigationBar'
import { Jumbotron } from './Components/Backgroundpic'
import {BicepCurls} from './WorkoutsPages/BicepCurls'
import {Dips} from './WorkoutsPages/Dips'
import {Situps} from "./WorkoutsPages/Sit-ups"

function App() {
  return (
    <React.Fragment>
      <NavigationBar/>
      <Jumbotron/>
      <Layout>
        <Router>
          <Switch>
            <Route exact path ="/" component = {Home} />
            <Route path ="/about" component = {About} />
            <Route path ="/recipes" component = {Recipes} />
            <Route path ="/workouts" component = {Workouts} />
            <Route path ="/healthnews" component = {HealthNews} />
            <Route path ="/bicepcurls" component = {BicepCurls} />
            <Route path ="/dips" component = {Dips} />
            <Route path ="/situps" component = {Situps} />
            <Route component = {NoMatch} />
          </Switch>
        </Router>
      </Layout>
    </React.Fragment>
  );
}

export default App;
