import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'

export const Situps = () => (
    <div>
        <h1>Sit-ups</h1>
        <hr></hr>
        <Container>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Category: Abs</p>
                </Col>
                <Col md = {3}>
                    <p>Benefits/Muscles Worked: Rectus abdominis</p>
                </Col>
                <Col md = {3}>
                    <p>Equipment needed: N/A</p>
                </Col>
            </Row>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Description:
                    Sit on a mat, your calves are resting on a bench, the knees make a right angle. 
                    Hold your hands behind your neck. Go now up with a rolling movement of your back, you 
                    should feel how the individual vertebrae lose contact with the mat. At the highest point, 
                    contract your abs as much as you can and hold there for 2 sec. Go now down, unrolling 
                    your back.</p>
                </Col>
                <Col>
                    
                </Col>
            </Row>
        </Container>
    </div>

)