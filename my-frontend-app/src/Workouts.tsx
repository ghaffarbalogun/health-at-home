import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import ButtonToolbar from 'react-bootstrap/esm/ButtonToolbar'
import { Link } from 'react-router-dom'

export const Workouts = () => (
    <div>
        <h1>Workouts</h1>
        <hr></hr>
        <h4>Find out about what possible excercises you can incorporate into your workout routine.</h4>
        <p>You can find the right workouts that fit your schedule whether you are a college student on a budget,
            a busy parent, or anyone in between!</p>
        <hr></hr>
        <Container>
            <Row className="text-center">
                <Col md = {3}>
                    <Link to= '/bicepcurls'> 
                        <ButtonToolbar>
                            <Button variant='success' size='lg' block>
                                Biceps Curls With Dumbbell
                            </Button>
                        </ButtonToolbar>
                    </Link>
                </Col>
                <Col md = {3}>
                    <Link to= '/dips'> 
                        <ButtonToolbar>
                            <Button variant='info' size='lg' block>
                                Dips
                            </Button>
                        </ButtonToolbar>
                    </Link>
                </Col>
                <Col md = {3}>
                    <Link to= '/situps'> 
                        <ButtonToolbar>
                            <Button variant='primary' size='lg' block>
                                Sit-ups
                            </Button>
                        </ButtonToolbar>
                    </Link>
                </Col>
            </Row>
        </Container>
    </div>

)
